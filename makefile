CXX      = g++
CXXFLAGS = -Wall -ansi -g --std=c++11
CC       = g++
CCFLAGS  = -g
OBJS     = test.o population.o unit.o

test : $(OBJS)
	$(CXX) -o $@ $(OBJS)

test.o : test.cpp
	$(CXX) -c $(CXXFLAGS) $<
population.o : population.cpp
	$(CXX) -c $(CXXFLAGS) $<
unit.o : unit.cpp
	$(CXX) -c $(CXXFLAGS) $<
