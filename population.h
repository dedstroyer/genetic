#ifndef POPULATION_H
#define POPULATION_H
#include "unit.h"
Data * combineData(Data* parent1,Data* parent2);
double calculateFitness(struct Data * result, struct Data * creature);

class population {	
	private:
	std::vector<unit> units;
	int mutation;	
	void addUnit(unit creature);
	void repopulate();
	void cut();
	unit* getUnit(int id);	
	void analyseFitness(struct Data* target);
	int getMutation();
	uint size();
	void sort();
	public:	
	population(){
		mutation=0;
	}
	void mutate();	
	void select(Data* target);
	void print();
	void populate(int size);
	void setMutation(int newMutation);
};
#endif
