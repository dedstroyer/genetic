#ifndef UNIT_H
#define UNIT_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <random>
void swapInt(int* a, int* b);
int randomInt(int a,int b);
struct Data{
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};
class unit {
	private:
		
	Data data;
	unsigned long id;
	double fitness;
	   
    public:
    unit(){
	fitness=0;
	data.red=0;
	data.green=0;
	data.blue=0;
	id=0;
	}
	double getFitness();
	void setFitness(double newFitness);
	void mutate(int mutateRation);	
	unsigned long getId();	
	struct Data * getData();
	void setData(Data* newData);
	void printData();
};
#endif
