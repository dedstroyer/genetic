#include <iostream>
#include "creature.h"
#include "compare.h"
#include "population.h"
int main(){
	Data target;
	printf("Введите R G B :");
	scanf("%d %d %d",&(target.red),&(target.green),&(target.blue));
    srand(time(0));
    population first;    
	first.setMutation(10);
    first.populate(100);
    first.analyseFitness(&target);
	first.print();
	for(int j=0;j<100;++j){
	printf("\nMutation %d\n",j);
	first.analyseFitness(&target);
	first.sort();
	first.print();
	first.cut(10);
	if(first.getUnit(0)->getFitness()<3){break;}
	first.repopulate();
	}
	return 1;
}
