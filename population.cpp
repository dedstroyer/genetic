#include "population.h"
Data * combineData(Data* parent1,Data* parent2){
	Data* data =(Data*) malloc(sizeof(Data));
	data->red = (parent1->red+parent2->red)/2;
	data->green = (parent1->green+parent2->green)/2;	
	data->blue = (parent1->blue+parent2->blue)/2; 
	return data;
}

double calculateFitness(struct Data * result, struct Data * creature)
{
	double dR = pow(result->red-creature->red,2);
	double dG = pow(result->green-creature->green,2);
	double dB = pow(result->blue-creature->blue,2);
	
	double newFitness=sqrt(dR + dG + dB);
	return newFitness;
}
	
	
	
	void population::addUnit(unit creature)
	{
		units.push_back(creature);
	}
	void population::setMutation(int newMutation){mutation=newMutation;}
	void population::mutate()
	{
		for(int i=0;unsigned(i)<units.size();++i)
			{
				units[i].mutate(mutation);
			}
	}
	
	void population::populate(int size)
	{
		for(int i=0;i<size;++i){
		unit creature;
		addUnit(creature);
		}
	}
	
	void population::repopulate()
	{
		uint size = units.size();
		for(uint i=0;i<size;++i)
		{
				unit creature;
				creature.setData(combineData(units[i].getData(),units[i+1].getData()));
				creature.mutate(mutation);
				addUnit(creature);
				creature.mutate(mutation);
				addUnit(creature);		
				i++;
		}
	}
	
	void population::cut()
	{
		
	}
	unit* population::getUnit(int id){return &(units[id]);}
	void population::print()
	{
		for(int i=0;unsigned(i)<units.size();++i)
			{
				units[i].printData();
			}
	}
	
	void population::analyseFitness(struct Data* target)
	{
		for(int i=0;unsigned(i)<units.size();++i)
			{
				units[i].setFitness(calculateFitness(target,units[i].getData()));
			}
	}
	int population::getMutation(){return mutation;}
	uint population::size(){return units.size();}
	void population::sort()
	{
		int i, j, flag = 1; 
		unit temp;
      int length = units.size(); 
      for(i = 1; (i <= length) && flag; i++)
     {
          flag = 0;
          for (j=0; j < (length -1); j++)
         {
               if (units[j+1].getFitness() < units[j].getFitness())
                  { 
                    temp = units[j];  
                    units[j] = units[j+1];
                    units[j+1] = temp;
                    flag = 1;
               }
          }
     }
	}
	void population::select(Data* target){
		analyseFitness(target);
		sort();
		}
