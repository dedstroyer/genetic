#include "unit.h"

	void swapInt(int* a, int* b){ int tmp = *a; *a=*b; *b=tmp; }
	int randomInt(int a,int b)
	{	
		if (b > a) swapInt(&a,&b);
		return ((rand() % (b-a+1)) + a);
	}
	void unit::setFitness(double newFitness)
	{
		fitness = newFitness;
      }
	double unit::getFitness(){return fitness;}
	void unit::mutate(int mutateRation)
	{
		int randColor = randomInt(-mutateRation,mutateRation);
		switch(randColor%3){
			case 0  : data.red+=randColor;break;
			case 1  : data.green+=randColor;break;
			case 2  : data.blue+=randColor;break;
		}
	}
	
	unsigned long unit::getId()
	{
		return id;
	}
	
	struct Data * unit::getData()
	{
		return &data;
	}
	void unit::setData(Data* newData)
	{
		data.red=newData->red;
		data.green=newData->green;
		data.blue=newData->blue;
	}
	void unit::printData()
	{
		printf("(%d,%d,%d) : %3.4lf\n",data.red,data.green,data.blue,fitness);
	}
